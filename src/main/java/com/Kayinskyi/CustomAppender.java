package com.Kayinskyi;
import java.io.Serializable;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

public class CustomAppender {

    // note: class name need not match the @Plugin name.
    @Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
    public final class SmsAppender extends AbstractAppender {
        protected SmsAppender(String name, Filter filter,
                              Layout<? extends Serializable> layout, final boolean ignoreExceptions) {
            super(name, filter, layout, ignoreExceptions);
        }

         //here I send SMS-message
        public void append(LogEvent event) {
            try {
                ExampleSMS.send(new String(getLayout().toByteArray(event)));
            } catch (Exception ex) {
            }
        }

    }
}
