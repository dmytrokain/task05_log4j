package com.Kayinskyi;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Plant {

    private static final Logger log = LogManager.getLogger(Plant.class.getName());

    private String name;
    private String size;

    public Plant(String name, String size){
        log.info("com.Kayinskyi.Plant created");
        this.name = name;
        this.size = size;
    }

    public void add(){
        log.error("warning");
    }

    public static void main(String[] args) {
        Plant plant = new Plant("chi", "big");
        plant.add();
        log.warn("Warn message");
        log.debug("Debug message");
    }
}
